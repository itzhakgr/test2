<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\User;
use app\models\Activity;
use yii\web\NotFoundHttpException;
use Yii; 

class OwnPostRule extends Rule
{

	public $name = 'OwnPostRule';

	public function execute($user, $item, $params)
	{
		if(!Yii::$app->user->isGuest)
		{
			return isset($params['post']) ? $params['post']->owner == $user : false;
		}
		/*
		if(isset($_GET['id']))
		{
			$userCategory = User::findOne($user);
			$activityCategory = Activity::findOne($_GET['id']);
			//������ �� ��� ����� �� ���� �� ����� �������� ��� ������ ������� ���� ����
			if(isset($userCategory) && isset($activityCategory))
			{
				if($userCategory->categoryId == $activityCategory->categoryId)
					return true;
			}
		}
		return false;
	}*/
	}
}